// Code for read file.

/*const fs = require("fs");
fs.readFile("./text.txt" , "utf8", (err , data)=>{
	if(err)
	console.log("err");
	console.log("data:" ,data);	
}
);*/

//code for create a server

var http = require('http');
var fs = require('fs');
var url = require('url');
var querystring = require('querystring');

var server = http.createServer((req , res)=>{
    //console.log("got a request" , req.url );
   //res.end("Sending a message");
   //if(req.method==="GET")
   if(req.url==="/form"){
   	  res.writeHead(200, {"Content-Type": "text/html"});
   	  fs.createReadStream("./node_imp.html"  , "UTF-8").pipe(res);
      // console.log("Calling via GET method");
   }
    if(req.method==="GET"){
    	//console.log("GET method");
    	var q = url.parse(req.url , true).query;
    	console.log(q);
    } 
    else if(req.method==="POST"){
      var data = "";
      req.on("data" , (a)=>{
      	 data+=a;
      });

     req.on("end" , (a)=>{
     	var form_data = querystring.parse(data);
     	 console.log(form_data);
     });
     
    }
  
});
server.listen(8003);
